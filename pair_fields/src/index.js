import InterfaceComponent from './interface.vue';

export default {
	id: 'pairs',
	name: 'Pairs',
	icon: 'box',
	description: 'This is my pairs interface!',
	component: InterfaceComponent,
	options: null,
	types: ['string'],
};
