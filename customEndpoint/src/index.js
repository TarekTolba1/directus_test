export default (router, {services ,exceptions}) => {
	const { ItemsService } = services;
	const { ServiceUnavailableException } = exceptions;
	router.get('/', (req, res) => {
				const lessonService = new ItemsService('lesson', { schema: req.schema, accountability: req.accountability });

		lessonService
			.readByQuery({ sort: ['name'], fields: ['*'] })
			.then((results) => res.json(results))
			.catch((error) => {
				return next(new ServiceUnavailableException(error.message));
			});
	});
};
